# FriendToGo

Kelompok 4 Tugas Akhir

Rekayasa Perangkat Lunak FASILKOM UI, Semester Genap 2017/2018

Repositori _git_ ini berisi kode sumber untuk aplikasi _FriendToGo_,
yang dibuat oleh kelompok 4 yang beranggotakan:

- 4A
  - Degoldie Sonny (1606862702)
  - Dennis Febri Dien (1606838193)
  - Firman Hadi Prayoga (1606862721)
  - Izzatul Muttaqin (1606837915)
  - Juan Alexander Hamonangan (1606839334)
  
- 4B
  - Azhar Kurnia (1606918471)
  - Nadhifah Hanan (1606890542)
  - Priambudi Lintang Bagaskara (1606895171)
  - Tyagita Larasati (1606918616)
  - Wildan Fahmi Gunawan (1606889591)
  
  
## Status

Berikut adalah informasi detil tentang _branch_ utama setiap kelompok:

- Kelompok kecil A ([_kel-a/master_](https://gitlab.com/rpl18-ta-4/friendtogo/tree/kel-a/master))
  - Situs: https://rpl18-ta-4-a.herokuapp.com/
  - [![pipeline status](https://gitlab.com/rpl18-ta-4/friendtogo/badges/kel-a/master/pipeline.svg)](https://gitlab.com/rpl18-ta-4/friendtogo/commits/kel-a/master)
  - [![coverage report](https://gitlab.com/rpl18-ta-4/friendtogo/badges/kel-a/master/coverage.svg)](https://gitlab.com/rpl18-ta-4/friendtogo/commits/kel-a/master)
  
- Kelompok kecil B ([_kel-b/master_](https://gitlab.com/rpl18-ta-4/friendtogo/tree/kel-b/master))
  - Situs: https://rpl18-ta-4-b.herokuapp.com/
  - [![pipeline status](https://gitlab.com/rpl18-ta-4/friendtogo/badges/kel-b/master/pipeline.svg)](https://gitlab.com/rpl18-ta-4/friendtogo/commits/kel-b/master)
  - [![coverage report](https://gitlab.com/rpl18-ta-4/friendtogo/badges/kel-b/master/coverage.svg)](https://gitlab.com/rpl18-ta-4/friendtogo/commits/kel-b/master)
  
- Gabungan ([_develop_](https://gitlab.com/rpl18-ta-4/friendtogo/tree/develop))
  - Situs: https://rpl18-ta-4-merged.herokuapp.com/
  - [![pipeline status](https://gitlab.com/rpl18-ta-4/friendtogo/badges/develop/pipeline.svg)](https://gitlab.com/rpl18-ta-4/friendtogo/commits/develop)
  - [![coverage report](https://gitlab.com/rpl18-ta-4/friendtogo/badges/develop/coverage.svg)](https://gitlab.com/rpl18-ta-4/friendtogo/commits/develop)
  
## Catatan Untuk Pengembang (Anggota Kelompok)

### Memulai untuk pertama kalinya

**Cobalah untuk me-_refresh_ pengetahuan Anda tentang _Django_ dari repositori praktikum PPW Anda.**
Versi dari _Django_ yang digunakan sama persis dengan versi yang digunakan pada praktikum (1.11.4). Disediakan juga
_Selenium_ bagi yang ingin melakukan _functional test_.

Anda bisa mulai pekerjaan Anda dengan mengubah isi dari _static, views, urls, templates_ yang ada pada aplikasi
``ftg_core``. Isi awal dari aplikasi tersebut merupakan contoh untuk memahami struktur proyek yang digunakan.

### Konvensi dalam proyek ini

Konvensi yang sudah jelas seperti penamaan variabel harus rapih dan dapat dimengerti orang lain tidak akan ditulis disini.
Harap kerjasamanya :)

- Seharusnya, Anda tidak perlu membuat aplikasi baru lagi (cukup ``ftg_core`` saja). Namun apabila terdapat kondisi
yang mengharuskan Anda untuk membuat aplikasi baru (_readability_, _maintainability_, _requirement_, dll) Anda dapat melakukan
diskusi dengan kelompok masing-masing terlebih dahulu.

  - Format nama untuk aplikasi adalah ``ftg_namaaplikasi`` dengan ``namaaplikasi`` sebagai nama aplikasi yang ingin Anda buat.
  **Penamaan ini sangat penting dilakukan**, karena aplikasi _coverage_ hanya akan menganalisa baris kode yang ada didalam aplikasi berawalan ``ftg_``.
  
- **Jangan langsung push ke _branch_ utama maupun _develop_ apalagi _master_!**
  
  - Buatlah _merge request_ ke _branch_ tersebut dan tunggu teman Anda me-_review_ pekerjaan Anda. Lakukan seperti
  Anda mengerjakan praktikum Pemrograman Lanjut (_Advanced Programming_).
  
- Format penamaan _branch_ fitur untuk setiap kelompok kecil adalah ``kel-X/namabranch`` dengan ``namabranch`` sebagai nama _branch_ yang ingin Anda buat.
  **Buatlah _branch_ fitur tersebut dari _branch_ utama kelompok kecil Anda!** Apabila Anda tidak mengerti caranya, buatlah _branch_ dengan perintah berikut:
  
   `git checkout -b kel-X/namabranch kel-X/master`
  
   Contoh, Fulan dari kelompok kecil A ingin membuat _branch_ fitur pendaftaran, maka Fulan akan memanggil perintah berikut:
  
   `git checkout -b kel-a/pendaftaran kel-a/master`
  
   **Salah membuat cabang dari _branch_ dapat menyebabkan _error_.**

- _CI Pipeline_ terdiri dari 2 fase: **_test_** dan **_deployment_**. Fase _test_ akan dijalankan di setiap _branch_, sedangkan _deployment_
hanya akan berjalan di 2 _branch_ utama dan _develop_.

- **Konvensi di atas dibuat untuk mempermudah dalam menyelesaikan tahap 1 dari proyek ini.**
Masih banyak hal lain yang harus diperhatikan, namun belum sempat ditulis disini.
Konvensi-konvensi tambahan akan ditulis di dalam grup LINE.

### Saran

- Untuk mempermudah integrasi, sebisa mungkin gunakan format penamaan ``namafitur_namaobjek`` untuk nama _template_,
_method view_, _static file_, dan lain-lain. 

- Bagi yang ingin membuat struktur kode yang rapih bisa gunakan IDE seperti _PyCharm_. IDE dapat menotifikasi _coding style_ yang salah
(tidak memenuhi _PEP 8_ : https://pep8.org/).


### Informasi lebih lanjut

Apabila ada permasalahan dalam _pipeline_ (_error_ dalam _deployment_, bukan _unit test_ yang gagal karena kesalahan pembuat kode),
Anda dapat menghubungi Firman.


_Good luck and have fun!_
-firmanhp